/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Action;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Mirko
 */
public class ReservarHotelalgorit {
    private HashMap <Date,Integer> reservas=new HashMap();
     
    public void reservar(Date desde,Date hasta,int personas){
        Date fecha=desde;
        Integer huespedes;
        Calendar cal=Calendar.getInstance();// suma fechas calenDAR
        Integer cantPlazas=50;
        while(fecha.equals(hasta)){
            
            huespedes=reservas.get(desde);
            if(huespedes==null){
                reservas.put(fecha, personas);
            }else{
                if(cantPlazas<personas+huespedes){
                    System.out.println("no se puede reservar");
                }else{
                    huespedes=huespedes+personas;
                    reservas.put(fecha, huespedes);
                }
            }
            cal.setTime(fecha);
            cal.add(Calendar.DATE,1);
            fecha=cal.getTime();
            
        }
         
     }
    
    public void recorrer(){
        
        Set<Date> keySet=reservas.keySet();
        Iterator <Date>ite=keySet.iterator();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
        while (ite.hasNext()) {
            Date fecha=ite.next();
            System.out.println(sdf.format(fecha)+ " "+reservas.get(fecha));
            
        }
    }
}
